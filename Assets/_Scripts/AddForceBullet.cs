﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForceBullet : MonoBehaviour
{
    [Header("Fireball speed")]
    public float moveSpeed = 2;
    float timer = 5f;

    void Update()
    {
        timer -= Time.deltaTime;
        transform.position += transform.forward * Time.deltaTime * moveSpeed;
        if(timer <= 0)
        {
            Destroy(gameObject);
        }
    }
}
