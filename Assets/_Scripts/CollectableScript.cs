﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CollectableScript : MonoBehaviour
{
    public Transform player;
    public WaveTrigger trigger;
    public PlayerController playerController;

    [Header("SoundFX")]
    public AudioSource audioSource;
    public AudioClip collect_FX;
    
    void MoveIntoGame()
    {
        transform.DOMoveY(player.transform.position.y, 0.2f);
    }

    void PickUp()
    {  
        DOTween.Kill(transform);
        transform.DOMove(player.transform.position, 1f);
        transform.DOScale(new Vector3(0,0,0), 0.5f).SetEase(Ease.InExpo);
        transform.DOLocalRotate(new Vector3(360, 0, 0), 1f);
    }

    void Update()
    {
        if(trigger.Triggered)
        {
            MoveIntoGame();
        }

        if(transform.localScale == Vector3.zero)
        {
            DOTween.Kill(transform);
            Destroy(gameObject);
            playerController.collectableCount ++;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Fireball")
        {
            PickUp();
            audioSource.PlayOneShot(collect_FX);
        }
    }
}
