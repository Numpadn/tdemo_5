﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveTrigger : MonoBehaviour
{
    public bool Triggered;

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player")
        {
            Triggered = true;
        }
    }
}
