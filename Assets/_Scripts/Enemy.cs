﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Enemy : MonoBehaviour
{
    public Transform player;
    public WaveTrigger trigger;

    public bool dieStuff;

    [Header("SoundFX")]
    public AudioSource audioSource;
    public AudioClip die;

    public PlayerController playerController;
    
    public void MoveIntoGame()
    {
        transform.DOMoveY(player.transform.position.y, 0.2f);
    }

    public void TakeDamage()
    {
        DOTween.Kill(transform);
        transform.DOScale(new Vector3(0,0,0), 0.4f).SetEase(Ease.InExpo);
        audioSource.PlayOneShot(die);
    }

    public void LookTowardPlayer()
    {
        Quaternion targetRot = Quaternion.LookRotation(player.position - transform.position, Vector3.up);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 0.05f);
    }

    void Update()
    {
        LookTowardPlayer();

        if(trigger.Triggered)
        {
            MoveIntoGame();
        }

        if(transform.localScale == Vector3.zero)
        {
            playerController.enemyKilled ++;
            DOTween.Kill(transform);
            Destroy(gameObject);
            dieStuff = true;
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Fireball")
        {
            TakeDamage();
        }
    }
}
