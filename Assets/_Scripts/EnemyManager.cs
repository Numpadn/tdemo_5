﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public List<Enemy> allEnemies = new List<Enemy>();
    public static EnemyManager instance;

    void Awake()
    {
        //this defines itself as the enemy manager
        instance = this;
    }

    //This function makes the integer EnemyCount accessable from anywhere
    public static int EnemyCount()
    {
        //every time this function is called, the enemyCount is returned
        return(instance.allEnemies.Count);
    }

    //This function adds (Registers) the enemies to the EnemyManager
    public static void Register(Enemy e)
    {
        instance.allEnemies.Add(e);
        //This sets the enemies as a child of the EnemyManager.
        e.transform.SetParent(instance.transform);
    }

    //This function removes (Deregisters) the enemies from the EnemyManager.
    public static void Deregister(Enemy e)
    {
        instance.allEnemies.Remove(e);
    }
}
