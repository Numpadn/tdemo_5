﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Cinemachine;

public class PlayerController : MonoBehaviour
{
    [Header("Managers")]
    public Crosshair crosshairManager;

    public int collectableCount = 0;
    public int enemyKilled = 0;

    [Header("Flying Properties")]
    //public float speed;
    //public float flyingSensitivity = 0.2f;
    public Transform rotTransform;
    public CinemachineDollyCart dollyCart;
    public float rotSpeed = 5;

    public bool boosting;
    public Vector3[] bRoll_waypoints;
    public float bRollSpeed;
    public float dollyCartSpeed = 50;

    public GameObject fireballPrefab;
    public float distance = 1000;

    public Transform nose;
    public Camera cam;

    public AudioSource playerSoundFX;
    public AudioClip playerShoot;




    //[Header("Dodge Properties")]
    //float for dodge speed
    //float for glideThreshold

    //[Header("Timer Properties")]
    //float for the cool down
    //float for how long the boost lasts
    //bool for checking boost has finished
    //bool for checking if player is cooling down


    public void TakeDamage(int damageDealt)
    {
        //play player hurt sound
        //take away player health
    }

    void Update()
    {
        Vector2 rotateAmount = crosshairManager.GetRotAmount();
        
        if(Input.GetMouseButtonDown(0))
        {
            var position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
            position = Camera.main.ScreenToWorldPoint(position);
            GameObject fireBall = Instantiate(fireballPrefab);
            fireBall.transform.position = nose.position;
            fireBall.transform.LookAt(position);
            playerSoundFX.PlayOneShot(playerShoot);

        }

        if(Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.D))
        {
            int dir = Input.GetKeyDown(KeyCode.D) ? -1 : 1;
            QuickSpin(dir);
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            BarrelRoll();
        }


        //If player is boosting
        if(boosting)
        {
            dollyCart.m_Speed = dollyCartSpeed + 10;
        }
        else
        {
            dollyCart.m_Speed = dollyCartSpeed;
        }

            //minus time from the boost length (duration of the boost)

            //if duration of boost less or equal to 0
                //set flight back down 

        //if cooldown from boost is true
            //count down how long boost is
            //if its 0
                //player can boost again
    }

    public void QuickSpin(int dir)
    {
        if(!DOTween.IsTweening(transform) && !DOTween.IsTweening(rotTransform))
        {
            transform.DOLocalRotate(new Vector3(0, 0, dir * 360), bRollSpeed, RotateMode.LocalAxisAdd);
        }
    }

    public void BarrelRoll()
    {
        if(!DOTween.IsTweening(rotTransform) && !DOTween.IsTweening(transform))
        {
            transform.DOLocalRotate(new Vector3(-360, 0, 0), bRollSpeed, RotateMode.LocalAxisAdd).SetEase(Ease.InOutSine);
            rotTransform.DOLocalPath(bRoll_waypoints, bRollSpeed, PathType.CatmullRom, PathMode.Full3D, 10).SetEase(Ease.InOutSine);
        } 
    }

    void HandleDodge()
    {
        
        //transform the player to the right by the dodge speed and time.delta
        //set the dodge speed to lineraly interpolate
    }
}
