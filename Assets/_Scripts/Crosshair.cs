﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour
{
    public float speed = 10f;
    RectTransform rectTransform;
    float screenBoundsX;
    float screenBoundsY;

    public GameObject cursor;
    public Canvas canvas;
    float posX;
    float posY;

    public Vector2 screenBounds;
    public Vector2 actualBounds;

    public float mouseMultiplier;
    public float deadzone;

    
 
    public Vector2 GetRotAmount()
    {
        float positionX = rectTransform.anchoredPosition.x / screenBounds.x;
        float positionY = rectTransform.anchoredPosition.y / screenBounds.y;
        return new Vector2(-positionY, positionX);
    }

    public Vector2 GetCrosshairPos()
    {
        float positionX = rectTransform.anchoredPosition.x + screenBounds.x;
        float positionY = rectTransform.anchoredPosition.y + screenBounds.y;
        return new Vector2(positionX / 800, positionY / (screenBounds.y * 2));
    }

    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        MouseControls();
        CalculateBounds();
        //Vector2 joystickInput = new Vector2(Input.GetAxis("Horizontal"), -Input.GetAxis("Vertical"));
        //MoveCrosshair(joystickInput);
        Vector2 mouseInput = MouseControls();
        MoveCrosshair(mouseInput * mouseMultiplier);
    }

    void MoveCrosshair(Vector2 move)
    {
        rectTransform.anchoredPosition += move * speed * Time.deltaTime;
        float clampX = Mathf.Clamp(rectTransform.anchoredPosition.x, -screenBounds.x, screenBounds.x);
        float clampY = Mathf.Clamp(rectTransform.anchoredPosition.y, -screenBounds.y, screenBounds.y);
        rectTransform.anchoredPosition = new Vector2(clampX, clampY); 
    }

    public Vector2 MouseControls()
    {
        Vector3 screenMid = new Vector3(Screen.width/2, Screen.height/2, 0);
        Vector3 mouseOffset = Input.mousePosition - screenMid;

        float x = Mathf.Clamp(mouseOffset.x/screenMid.x, -1, 1);
        float y = Mathf.Clamp(mouseOffset.y/screenMid.y, -1, 1);
        Vector2 screenOffset = new Vector2 (x, y);
        
        Vector2 mouseCanvasPos = new Vector2 (screenOffset.x * actualBounds.x, screenOffset.y * actualBounds.y);
        Vector2 differenceMouse = mouseCanvasPos - rectTransform.anchoredPosition;
        
        float distanceToMove = differenceMouse.magnitude;
        if(distanceToMove <= deadzone)
        {
            return(Vector2.zero);
        }
        return(differenceMouse.normalized);
    }

    // void DetectInputMode()
    // {
    //     Vector2 joystickInput = new Vector2(Input.GetAxis("Horizontal"), -Input.GetAxis("Vertical"));
    //     if(mouseMove.hasMovedThisFrame)
    //     {
    //         inputMode = InputMode.mouse;
    //     }
    //     if(joystickInput.magnitude >= 0.1f)
    //     {
    //         inputMode = InputMode.joystick;
    //     }
    // }

    void CalculateBounds()
    {
        float screenWidth = 800f;
        float aspectRatio = (float)Screen.width / Screen.height;
        float screenHeight = screenWidth / aspectRatio;
        screenBounds = new Vector2 (screenWidth/2.2f, screenHeight/2.2f);
        actualBounds = new Vector2 (screenWidth/2f, screenHeight/2f);
    }

}
