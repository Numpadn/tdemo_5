﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public TextMeshProUGUI collectText;
    public TextMeshProUGUI enemyText;
    public PlayerController player;

    public Sprite crosshairIdle;
    public Sprite crosshairShoot;

    public Image crosshair;

    void Start()
    {
        Screen.SetResolution(256, 224, true);
    }
    
    void Update()
    {
        string collectString = player.collectableCount.ToString();
        collectText.text = collectString;

        string enemyString = "Enemies killed: " + player.enemyKilled.ToString();
        enemyText.text = enemyString;

        if(Input.GetMouseButton(0))
        {
            crosshair.sprite = crosshairShoot;
        }
        else
        {
            crosshair.sprite = crosshairIdle;
        }

        if(player.enemyKilled == 35)
        {
            SceneManager.LoadScene("Win");
        }
        
    }


}
